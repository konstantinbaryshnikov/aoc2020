const M = 32;

const INIT = 15;

export class Grid {

    constructor(initial) {
        this.grid = Array.from({length: M}).map(() => Array.from({length: M}).map(() => Array.from({length: M}).map(() => 0)));
        let z = INIT;
        for (let y = 0; y < initial.length; y++) {
            let line = initial[y];
            for (let x = 0; x < line.length; x++) {
                if (line[x]) {
                    this.set(true, z, y + INIT, x + INIT);
                }
            }
        }
    }

    step() {
        const grid = this.cloneGrid();
        for (let z = 0; z < M; z++) {
            for (let y = 0; y < M; y++) {
                for (let x = 0; x < M; x++) {
                    const n = this.countN(x, y, z);
                    if (this.grid[z][y][x]) {
                        if (n == 2 || n == 3) {
                            // pass
                        } else {
                            grid[z][y][x] = 0;
                        }
                    } else {
                        if (n == 3) {
                            grid[z][y][x] = 1;
                        } else {
                            // pass
                        }
                    }
                }
            }
        }
        this.grid = grid;
    }

    count() {
        let c = 0;
        for (let z = 0; z < M; z++) {
            for (let y = 0; y < M; y++) {
                for (let x = 0; x < M; x++) {
                    c += this.grid[z][y][x];
                }
            }
        }
        return c;
    }

    countN(ax, ay, az) {
        let c = 0;
        for (let z = Math.max(0, az - 1); z <= Math.min(M - 1, az + 1); z++) {
            for (let y = Math.max(0, ay - 1); y <= Math.min(M - 1, ay + 1); y++) {
                for (let x = Math.max(0, ax - 1); x <= Math.min(M - 1, ax + 1); x++) {
                    if (x == ax && y == ay && z == az) continue;
                    c += this.grid[z][y][x];
                }
            }
        }
        return c;
    }

    cloneGrid() {
        return this.grid.map(z => z.map(y => Array.from(y)));
    }

    set(value, x, y, z) {
        if (x < 0 || x >= M) throw new Error("x out of bounds: " + x);
        if (y < 0 || y >= M) throw new Error("y out of bounds: " + y);
        if (z < 0 || z >= M) throw new Error("z out of bounds: " + z);
        this.grid[z][y][x] = value | 0;
    }

}
