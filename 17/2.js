import {readFile} from '../common/input.js';
import {Grid} from './grid4.js';

const d = readFile('./input', s => s.trim().split('').map(c => c === '#'));
const grid = new Grid(d);
for (let i = 0; i < 6; i++) {
    console.log(i);
    grid.step();
}
console.log(grid.count());
