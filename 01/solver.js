export const findPair = (a, s, i, j) => {
    a.sort((a, b) => a - b);
    i = i || 0;
    j = j || a.length - 1;

    while (i < j) {
        const sum = a[i] + a[j];
        if (sum == s) {
            return a[i] * a[j];
        }
        if (sum < s) {
            i++;
        } else {
            j--;
        }
    }
};

export const findTriplet = (a, s) => {
    let i = 0;
    while (i < ((a.length / 2) | 0)) {
        const result = findPair(a, s - a[i], i + 1);
        if (result) {
            return result * a[i];
        }
        i++;
    }
};
