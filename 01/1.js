import {findPair} from './solver.js';
import {readFile} from '../common/input.js';

const input = readFile('input', parseInt);
const result = findPair(input, 2020);
console.log(result);
