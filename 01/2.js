import {findTriplet} from './solver.js';
import {readFile} from '../common/input.js';

const input = readFile('input', parseInt);
const result = findTriplet(input, 2020);
console.log(result);
