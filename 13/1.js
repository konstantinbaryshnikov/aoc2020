import {readInput} from './input.js';

let {ts, buses} = readInput();
buses = buses.filter(id => id);
const wait = buses.map(id => [id, id - (ts % id)]);
let min = [0, 999999999];
for (let w of wait) {
    if (w[1] < min[1]) {
        min = w;
    }
}
console.log(min[0] * min[1]);
