import {readFile} from '../common/input.js';

export const readInput = () => {
    const lines = readFile('./input', s => s.trim());
    return {
        ts: lines[0] | 0,
        buses: lines[1].split(',').map(s => s === 'x' ? undefined : s | 0),
    };
};
