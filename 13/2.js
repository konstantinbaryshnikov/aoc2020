import {readInput} from './input.js';

let buses = readInput().buses.map((v, i) => [v, i]).filter(v => v[0]);

function test(value, slice) {
    for (const bus of slice) {
        if ((value + bus[1]) % bus[0] !== 0) {
            return false;
        }
    }
    return true;
}

let value = 0;
for (let i = 0; i < buses.length; i++) {
    const slice = buses.slice(0, i + 1);
    const step = buses.slice(0, i).reduce((a, b) => a * b[0], 1);
    while (!test(value, slice)) {
        value += step;
    }
}

console.log(value);
