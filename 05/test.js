import {parseBsp} from './bsp.js';

function test1() {
    const {row, col, id} = parseBsp("FBFBBFFRLR");
    console.assert(row == 44, 44);
    console.assert(col == 5, 5);
    console.assert(id == 357, 357);
}

test1();
