if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function(str, newStr){

		// If a regex pattern
		if (Object.prototype.toString.call(str).toLowerCase() === '[object regexp]') {
			return this.replace(str, newStr);
		}

		// If a string
		return this.replace(new RegExp(str, 'g'), newStr);

	};
}

export const parseRow = s => {
    return parseInt(s.replaceAll('F', '0').replaceAll('B', '1'), 2);
};

export const parseCol = s => {
    return parseInt(s.replaceAll('L', '0').replaceAll('R', '1'), 2);
};

export const parseBsp = s => {
    const rowSpec = s.substr(0, 7);
    const colSpec = s.substr(7);
    const row = parseRow(rowSpec);
    const col = parseCol(colSpec);
    return {row, col, id: row * 8 + col};
};
