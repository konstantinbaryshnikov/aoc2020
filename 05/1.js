import {parseBsp} from './bsp.js';
import {readFile} from '../common/input.js';

const input = readFile('./input', s => s.trim());
let max = 0;
for (const spec of input) {
    const {id} = parseBsp(spec);
    max = Math.max(id, max);
}
console.log(max);
