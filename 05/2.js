import {parseBsp} from './bsp.js';
import {readFile} from '../common/input.js';

const input = readFile('./input', s => s.trim());
const ids = [];
for (const spec of input) {
    const {id} = parseBsp(spec);
    ids.push(id);
}
ids.sort((a, b) => a - b);

for (let i = 1; i < ids.length - 1; i++) {
    if (ids[i - 1] != ids[i] - 1) {
        console.log(ids[i] - 1);
        break;
    }
}
