import * as fs from 'fs';
import {RuleCompiler} from './match.js';

const lines = [];
const rules = [];
let mode = 0;
fs.readFileSync('./input', 'utf8').split(/\n/).forEach(s => {
    s = s.trim();
    if (s === '' && mode == 0) {
        mode = 1;
        return;
    }
    if (mode) {
        lines.push(s);
    } else {
        rules.push(s);
    }
});

const c = new RuleCompiler(rules);
const r = c.compile();

console.log(lines.reduce((c, s) => c + (r.test(s) ? 1 : 0), 0))
