export class RuleCompiler {

    constructor(s) {
        this.src = [];
        this.known = [];
        for (let line of s) {
            let tokens = line.trim().split(': ');
            let i = tokens[0] | 0;
            let m = tokens[1].match(/^"([a-z])"$/);
            if (m) {
                this.known[i] = m[1];
            } else {
                let t = tokens[1].trim().split("|").map(s => s.split(" ").filter(v => v.trim().length).map(v => v | 0));
                this.src[i] = t;
            }
        }
    }

    compile() {
        return new RegExp('^' + this.get(0) + '$');
    }

    get(i) {
        if (this.known[i] == null) {
            this.known[i] = this.compileRule(i);
        }
        return this.known[i];
    }

    compileRule(i) {
        const groups = [];
        for (let opts of this.src[i]) {
            const group = [];
            for (let j of opts) {
                group.push(this.get(j));
            }
            groups.push(group.join(''));
        }
        return '(' + groups.join('|') + ')';
    }

}

function test1() {
    const s = `0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"`.split(/\n/);
    const c = new RuleCompiler(s);
    const r = c.compile();
    const ss = `ababbb
bababa
abbbab
aaabbb
aaaabbb`.split(/\n/);
    for (let s of ss) {
        console.log(s, r.test(s));
    }
}
//test1();
