export class RuleCompiler {

    constructor(s, overrides) {
        this.src = [];
        this.known = [];
        for (let line of s) {
            let tokens = line.trim().split(': ');
            let i = tokens[0] | 0;
            let m = tokens[1].match(/^"([a-z])"$/);
            if (m) {
                this.known[i] = m[1];
            } else {
                let ss = overrides[i] || tokens[1];
                let t = ss.trim().split("|").map(s => s.split(" ").filter(v => v.trim().length).map(v => {
                    return v[0] >= 0 ? (v | 0) : v;
                }));
                this.src[i] = t;
            }
        }
    }

    compile() {
        return new RegExp('^' + this.get(0) + '$');
    }

    get(i) {
        if (this.known[i] == null) {
            this.known[i] = this.compileRule(i);
        }
        return this.known[i];
    }

    compileRule(i) {
        const groups = [];
        for (let opts of this.src[i]) {
            const group = [];
            for (let j of opts) {
                if (typeof j === 'string') {
                    let r = this.get(j.substr(1) | 0);
                    group.push('(' + r + ')' + j[0]);
                } else {
                    group.push(this.get(j));
                }
            }
            groups.push(group.join(''));
        }
        return '(' + groups.join('|') + ')';
    }

}
