import * as fs from 'fs';
import {RuleCompiler} from './match2.js';
const lines = [];
const rules = [];
let mode = 0;
fs.readFileSync('./input', 'utf8').split(/\n/).forEach(s => {
    s = s.trim();
    if (s === '' && mode == 0) {
        mode = 1;
        return;
    }
    if (mode) {
        lines.push(s);
    } else {
        rules.push(s);
    }
});

let o = [];
for (let i = 1; i < 7; i++) {
    o.push("42 ".repeat(i) + "31 ".repeat(i));
}

const overrides = {
    "8": "+42",
    "11": o.join("| ")
};

const c = new RuleCompiler(rules, overrides);
const r = c.compile();

console.log(lines.reduce((c, s) => c + (r.test(s) ? 1 : 0), 0))
