import {load, loadMonster} from './input.js';
import {connectTiles, Map} from './tile.js';

const tiles = load();
connectTiles(tiles);
const edge = tiles.filter(tile => tile.adj.filter(v => v === undefined).length == 2);
console.assert(edge.length == 4);
// Part 1 solution
console.log(edge.reduce((c, e) => c * e.id, 1));

const startTile = tiles.find(tile => !tile.adj[0] && !tile.adj[1]);
const monster = loadMonster();
const map = new Map(startTile.buildMap());
map.find(() => {
    if (!map.findMonsters(monster)) {
        return false;
    }
    console.log(map.count1());
    return true;
});
