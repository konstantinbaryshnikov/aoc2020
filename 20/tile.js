const cornerFn = [
    (i, len) => [i, 0], // top
    (i, len) => [0, i], // left
    (i, len) => [i, len - 1], // bottom
    (i, len) => [len - 1, i], // right
];

const oppositeCorner = i => (i + 2) % 4;

export function connectTiles(tiles) {
    findConnections(tiles, tiles[0]);
}

function findConnections(tiles, tile) {
    for (let c = 0; c < 4; c++) {
        if (tile.adj[c]) {
            continue;
        }
        let cv = tile.corner(c);
        let oc = oppositeCorner(c);
        for (let i = 0; i < tiles.length; i++) {
            const t = tiles[i];
            if (t.id === tile.id || t.adj[oc]) {
                continue;
            }
            if (t.find(() => t.corner(oc) == cv)) {
                tile.adj[c] = t;
                t.adj[oc] = tile;
                findConnections(tiles, t);
            }
        }
    }
}

export class Transformable {

    constructor(map) {
        console.assert(map.length === map[0].length, 'map is expected to have equal width and height');
        this.map = map;
        this.len = map.length;
        this.fixed = false;

        this.searchSteps = [
            () => this.rotate(),
            () => this.flipX(),
            () => this.flipY(),
            () => this.flipX(),
            () => this.flipY(),
        ];
    }

    find(fn) {
        if (this.fixed) {
            return fn();
        }
        for (let i = 0; i < 4; i++) {
            for (let step of this.searchSteps) {
                if (fn()) {
                    return this.fixed = true;
                }
                step();
            }
        }
        return false;
    }

    rotate() {
        this.transform((x, y) => this.map[this.len - 1 - x][y]);
    }

    flipX() {
        this.transform((x, y) => this.map[y][this.len - 1 - x]);
    }

    flipY() {
        this.transform((x, y) => this.map[this.len - 1 - y][x]);
    }

    transform(fn) {
        const map = [];
        for (let y = 0; y < this.len; y++) {
            for (let x = 0; x < this.len; x++) {
                if (!map[y]) {
                    map[y] = [];
                }
                map[y][x] = fn(x, y);
            }
        }
        this.map = map;
    }

}

export class Tile extends Transformable {

    constructor(id, map) {
        super(map);
        this.id = id;
        this.adj = Array.from({length: 4});

        this.fillMapStep = [
            (x, y) => [x, y - (this.len - 2)], // top
            (x, y) => [x - (this.len - 2), y], // left
            (x, y) => [x, y + (this.len - 2)], // bottom
            (x, y) => [x + (this.len - 2), y], // right
        ];
    }

    buildMap() {
        return this.fillMap([], 0, 0);
    }

    fillMap(map, x, y) {
        for (let dy = 0; dy < this.len - 2; dy++) {
            const my = y + dy;
            if (!map[my]) {
                map[my] = [];
            }
            for (let dx = 0; dx < this.len - 2; dx++) {
                const mx = x + dx;
                map[my][mx] = this.map[dy + 1][dx + 1];
            }
        }
        this.visited = true;
        for (let i = 0; i < 4; i++) {
            if (this.adj[i] && !this.adj[i].visited) {
                const nextTile = this.adj[i];
                const [nx, ny] = this.fillMapStep[i](x, y);
                nextTile.fillMap(map, nx, ny);
            }
        }
        return map;
    }

    corner(c) {
        const fn = cornerFn[c];
        let sum = 0;
        for (let i = 0; i < this.len; i++) {
            const [x, y] = fn(i, this.len);
            if (this.map[y][x]) {
                sum |= (1 << i);
            }
        }
        return sum;
    }

}

export class Map extends Transformable {

    constructor(map) {
        super(map);
    }

    findMonsters(monster) {
        let count = 0;
        for (let y = 0; y < this.len - monster.height; y++) {
            for (let x = 0; x < this.len - monster.width; x++) {
                if (this.testMonsterAt(x, y, monster)) {
                    count++;
                }
            }
        }
        return count;
    }

    count1() {
        let count = 0;
        for (let y = 0; y < this.len; y++) {
            for (let x = 0; x < this.len; x++) {
                if (this.map[y][x] == 1) {
                    count++;
                }
            }
        }
        return count;
    }

    testMonsterAt(x, y, monster) {
        let coords = [];
        for (let my = 0; my < monster.height; my++) {
            for (let mx = 0; mx < monster.width; mx++) {
                if (monster.map[my][mx]) {
                    if (!this.map[y + my][x + mx]) {
                        return false;
                    } else {
                        coords.push([x + mx, y + my]);
                    }
                }
            }
        }
        for (let [x, y] of coords) {
            this.map[y][x] = 2;
        }
        return true;
    }

}

export class Monster {

    constructor(map) {
        this.map = map;
        this.height = map.length;
        this.width = map[0].length;
    }

}
