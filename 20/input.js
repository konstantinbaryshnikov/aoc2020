import {Tile, Monster} from './tile.js';
import {readFile} from '../common/input.js';

export const load = () => {
    const tiles = [];
    let tile, m;
    const addTile = () => tiles.push(new Tile(tile.id, tile.map));
    const lines = readFile('./input', s => s.trim());
    for (let line of lines) {
        if (m = line.match(/Tile (\d+):/)) {
            tile && addTile();
            tile = {id: m[1] | 0, map: []};
        } else {
            tile.map.push(line.split('').map(c => c === '#' ? 1 : 0));
        }
    }
    addTile();
    return tiles;
};

export const loadMonster = () => {
    const monsterMap = [
        '                  # ',
        '#    ##    ##    ###',
        ' #  #  #  #  #  #   ',
    ].map(line => line.split('').map(c => c === '#' ? 1 : 0));
    return new Monster(monsterMap);
};
