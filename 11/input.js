import {Layout} from './layout.js';
import {readFile} from '../common/input.js';
export const readLayout = () => new Layout(readFile('./input', s => s.split('')));
