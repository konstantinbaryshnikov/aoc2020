export class Layout {

    constructor(grid) {
        this.grid = grid;
        this.width = this.grid[0].length;
        this.height = this.grid.length;
    }

    process() {
        const newGrid = this.grid.map(row => Array.from(row));
        let changed = false;
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                const seat = this.grid[y][x];
                if (seat === Layout.FLOOR) {
                    continue;
                }
                const cnt = this.countAdjOccupied(x, y);
                if (seat === Layout.EMPTY && cnt === 0) {
                    newGrid[y][x] = Layout.OCCUPIED;
                    changed = true;
                } else if (seat === Layout.OCCUPIED && cnt > Layout.MAX_ADJ_OCCUPIED) {
                    newGrid[y][x] = Layout.EMPTY;
                    changed = true;
                }
            }
        }
        if (!changed) {
            return false;
        }
        this.grid = newGrid;
        return true;
    }

    process2() {
        const newGrid = this.grid.map(row => Array.from(row));
        let changed = false;
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                const seat = this.grid[y][x];
                if (seat === Layout.FLOOR) {
                    continue;
                }
                const cnt = this.countVisibleOccupied(x, y);
                if (seat === Layout.EMPTY && cnt === 0) {
                    newGrid[y][x] = Layout.OCCUPIED;
                    changed = true;
                } else if (seat === Layout.OCCUPIED && cnt > Layout.MAX_VISIBLE_OCCUPIED) {
                    newGrid[y][x] = Layout.EMPTY;
                    changed = true;
                }
            }
        }
        if (!changed) {
            return false;
        }
        this.grid = newGrid;
        return true;
    }

    countOccupied() {
        let r = 0;
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                const seat = this.grid[y][x];
                if (seat === Layout.OCCUPIED) {
                    r++;
                }
            }
        }
        return r;
    }

    countAdjOccupied(x, y) {
        let r = 0;
        for (let cx = Math.max(0, x - 1); cx <= Math.min(x + 1, this.width - 1); cx++) {
            for (let cy = Math.max(0, y - 1); cy <= Math.min(y + 1, this.height - 1); cy++) {
                if (cx === x && cy === y) {
                    continue;
                }
                if (this.grid[cy][cx] === Layout.OCCUPIED) {
                    r++;
                }
            }
        }
        return r;
    }

    countVisibleOccupied(x, y) {
        const directions = [
            [-1, -1],
            [-1, 0],
            [-1, 1],
            [0, -1],
            [0, 1],
            [1, -1],
            [1, 0],
            [1, 1],
        ];
        let r = 0;
        const isInBounds = (x, y) => x >= 0 && x < this.width && y >= 0 && y < this.height;
        for (let [dx, dy] of directions) {
            for (let cx = x + dx, cy = y + dy; isInBounds(cx, cy); cx += dx, cy += dy) {
                if (this.grid[cy][cx] === Layout.OCCUPIED) {
                    r++;
                }
                if (this.grid[cy][cx] !== Layout.FLOOR) {
                    break;
                }
            }
        }
        return r;
    }

    print() {
        console.log(this.grid.map(s => s.join('')).join("\n") + "\n");
    }

}

Layout.FLOOR = '.';
Layout.EMPTY = 'L';
Layout.OCCUPIED = '#';
Layout.MAX_ADJ_OCCUPIED = 3;
Layout.MAX_VISIBLE_OCCUPIED = 4;
