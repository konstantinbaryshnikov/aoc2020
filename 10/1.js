import {readFile} from '../common/input.js';
const input = readFile('./input', v => v | 0);
input.sort((a, b) => a - b);
input.unshift(0);
input.push(input[input.length - 1] + 3);
let v1 = 0, v3 = 0;
for (let i = 1; i < input.length; i++) {
    const d = input[i] - input[i-1];
    if (d == 3) v3++;
    else if (d == 1) v1++;
}
const r = v1 * v3;
console.log(r);
