import {readFile} from '../common/input.js';

const input = readFile('./input', v => v | 0);
input.sort((a, b) => a - b);

let p = [1];

for (let v of input) {
    let iv = 0;
    for (let j = 1; j <= 3; j++) {
        iv += (p[v - j] || 0);
    }
    p[v] = iv;
}

console.log(p[p.length - 1]);
