import {VM} from './vm.js';

const code = `nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`.split("\n");

const vm = VM.loadBootcode(code);
vm.run(state => {
    if (state.visits.get(state.ptr)) {
        console.log(state.visits, state.acc, state.ptr);
        return false;
    }
});
