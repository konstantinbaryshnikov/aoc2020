import {readFile} from '../common/input.js';
import {VM} from './vm.js';

const code = readFile('./input', s => s.trim());

const vm = VM.loadBootcode(code);
vm.run(state => {
    if (state.visits.get(state.ptr)) {
        console.log(state.acc);
        return false;
    }
});
