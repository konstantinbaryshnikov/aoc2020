import {readFile} from '../common/input.js';
import {VM} from './vm.js';

const code = readFile('./input', s => s.trim());

const vm = VM.loadBootcode(code);

let p = 0;
let looping;

const swap = () => {
    const op = vm.state.instructions[p];
    const arg = op.arg;
    if (op instanceof VM.instructions.nop) {
        vm.state.instructions[p] = new VM.instructions.jmp(arg);
    } else if (op instanceof VM.instructions.jmp) {
        vm.state.instructions[p] = new VM.instructions.nop(arg);
    } else {
        return false;
    }
    return true;
};

do {
    looping = false;
    vm.state.reset();

    while (!swap()) {
        p++;
    }

    vm.run(state => {
        if (state.visits.get(state.ptr)) {
            looping = true;
            return false;
        }
    });

    if (looping) {
        swap();
        p++;
    }

} while (looping);
console.log(vm.state.acc);
