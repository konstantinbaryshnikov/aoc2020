export class VM {

    constructor(state) {
        this.state = state;
    }

    run(stepCb) {
        while (true) {
            const op = this.state.instructions[this.state.ptr];
            if (!op) {
                return;
            }
            if (stepCb(this.state) === false) {
                return;
            }
            const ptr = op.execute(this.state);
            this.state.visits.set(this.state.ptr, true);
            if (ptr !== undefined) {
                this.state.ptr = ptr;
            } else {
                this.state.ptr++;
            }
        }
    }

}

class State {
    constructor(instructions) {
        this.instructions = instructions;
        this.reset();
    }
    reset() {
        this.visits = new Map();
        this.acc = 0;
        this.ptr = 0;
    }
}

class Instruction {
    constructor(arg) {
        this.arg = arg;
    }
    execute(state) {
        throw new Error("abstract");
    }
}

class Nop extends Instruction {
    execute(state) {
        // nop
    }
}

class Acc extends Instruction {
    execute(state) {
        state.acc += this.arg;
    }
}

class Jmp extends Instruction {
    execute(state) {
        return state.ptr + this.arg;
    }
}

VM.instructions = {
    nop: Nop,
    acc: Acc,
    jmp: Jmp,
};

VM.loadBootcode = lines => {
    const instructions = [];
    for (let line of lines) {
        let [op, arg] = line.split(' ');
        arg = arg.replace('+', '') | 0;
        instructions.push(new VM.instructions[op](arg));
    }
    return new VM(new State(instructions));
};
