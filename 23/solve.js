const inputToArray = input => input.split('').map(v => v | 0);

const toLinkedList = (items, limit) => {
    let ptr = {value: items[0]};
    const map = new Map();
    map.set(ptr.value, ptr);
    const first = ptr;
    for (let i = 1; i < items.length; i++) {
        ptr.next = {value: items[i]};
        ptr = ptr.next;
        map.set(ptr.value, ptr);
    }
    for (let value = items.length + 1; value <= limit; value++) {
        ptr.next = {value};
        ptr = ptr.next;
        map.set(ptr.value, ptr);
    }
    ptr.next = first;
    return {ptr: first, map};
};

const buildState = (items, limit) => ({...toLinkedList(items, limit || items.length), min: 1, max: limit || items.length});

const exportValues = (ptr, limit) => {
    const start = ptr, values = [];
    do {
        values.push(ptr.value);
        ptr = ptr.next;
    } while (ptr !== start && (limit === undefined || values.length < limit));
    return values;
};

const exportValuesStartingWith = (value, state, limit) => {
    return exportValues(state.map.get(value), limit);
};

const playRound = state => {
    const cDest = dest => dest >= state.min ? dest : state.max;

    const src = state.ptr;

    let ptr = src.next;

    const picked = [];
    while (picked.length < 3) {
        picked.push(ptr);
        ptr = ptr.next;
        src.next = ptr;
    }

    let dest = cDest(src.value - 1);
    while (picked.indexOf(state.map.get(dest)) >= 0) {
        dest = cDest(dest - 1);
    }

    ptr = state.map.get(dest);

    picked[2].next = ptr.next;
    ptr.next = picked[0];

    state.ptr = src.next;
};

const play = (state, rounds) => {
    while (rounds-- > 0) {
        playRound(state);
    }
};

const part1 = input => {
    const rounds = 100;
    const state = buildState(inputToArray(input));
    play(state, rounds);
    console.log(exportValuesStartingWith(1, state).join('').substr(1));
};

const part2 = input => {
    const rounds = 10_000_000;
    const state = buildState(inputToArray(input), 1_000_000);
    play(state, rounds);
    const [, a, b] = exportValuesStartingWith(1, state, 3);
    console.log(a * b);
};

const input = '156794823';
part1(input);
part2(input);
