import * as fs from 'fs';

export const readFile = (filename, lineParser, sep) => fs.readFileSync(filename, 'utf8').split(sep || /\r?\n/).filter(s => s.trim().length).map(v => lineParser(v));
