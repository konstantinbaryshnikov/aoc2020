import {readInput} from './input.js';
import {isValid2} from './passport.js';

const input = readInput();

let validCount = 0;
for (let passport of input) {
    if (isValid2(passport)) {
        validCount++;
    }
}

console.log(validCount);
