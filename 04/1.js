import {readInput} from './input.js';
import {isValid} from './passport.js';

const input = readInput();

let validCount = 0;
for (let passport of input) {
    if (isValid(passport)) {
        validCount++;
    }
}

console.log(validCount);
