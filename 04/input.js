import {readFile} from '../common/input.js';

export const parseInput = s => {
    return s.split(/\s+/).filter(s => s.length).reduce((a, l) => {
        const [k, v] = l.split(':');
        a[k] = v;
        return a;
    }, {});
};

export const readInput = () => readFile('./input', parseInput, /\r?\n\r?\n/);
