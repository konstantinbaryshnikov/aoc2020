export const fields = {
    'byr': v => v >= 1920 && v <= 2002,
    'iyr': v => v >= 2010 && v <= 2020,
    'eyr': v => v >= 2020 && v <= 2030,
    'hgt': v => {
        const m = v.match(/^([0-9]+)(cm|in)$/);
        return m && {
            'cm': v => v >= 150 && v <= 193,
            'in': v => v >= 59 && v <= 76,
        }[m[2]](m[1]);
    },
    'hcl': v => v.match(/^#[a-f0-9]{6}$/),
    'ecl': v => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].indexOf(v) >= 0,
    'pid': v => v.match(/^[0-9]{9}$/),
    //'cid',
};

export const isValid = (passport) => {
    return !Object.keys(fields).some(field => !(field in passport));
};

const isValidField = (field, passport) => {
    if (!(field in passport)) {
        return false;
    }
    return fields[field](passport[field]);
};

export const isValid2 = (passport) => {
    return !Object.keys(fields).some(field => !isValidField(field, passport));
};
