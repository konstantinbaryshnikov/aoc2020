import {readFile} from '../common/input.js';
import {calc} from './calc.js';

const exprs = readFile('./input', s => s.trim());
let s = 0;
for (let e of exprs) {
    s += calc(e);
}
console.log(s);
