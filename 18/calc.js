const prec = {
    '(': 0,
    ')': 0,
    //'+': 1, // for part 1
    '+': 2,
    '-': 1,
    '*': 1,
    '/': 1,
}

export function parse(e) {
    const t = e.split(/(\s+|(?!=\()|(?=\)))/).filter(t => t.trim().length);
    const q = [];
    const s = [];
    let l;
    for (let o of t) {
        const n = parseInt(o, 10);
        if (!isNaN(n)) {
            q.push(n);
            continue;
        }
        if (o === ')') {
            while ('(' !== (l = s.pop())) {
                q.push(l);
            }
        } else if (o === '(') {
            s.push(o);
        } else {
            while (s.length && prec[o] <= prec[s[s.length - 1]]) {
                q.push(s.pop());
            }
            s.push(o);
        }
    }
    while (undefined !== (l = s.pop())) {
        q.push(l);
    }
    return q;
}

const operators = '+-*/'.split('');
export function evl(d) {
    let s = [];
    let cur;
    for (let t of d) {
        if (operators.indexOf(t) >= 0) {
            const b = s.pop();
            const a = s.pop();
            s.push(eval("" + a + t + b));
        } else {
            s.push(t);
        }
    }
    return s.pop();
}

export const calc = s => evl(parse(s));

function test1() {
    const tests = [
        ['2 * 3 + (4 * 5)', 26],
        ['5 + (8 * 3 + 9 + 3 * 4 * 3)', 437],
        ['5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', 12240],
        ['((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', 13632],
    ];
    for (let [e, r] of tests) {
        console.assert(r === calc(e), e + ' = ' + r);
    }
}
