export const parseGroup = answers => answers.join('').split('').reduce((a, c) => a.add(c), new Set());

export const parseGroup2 = answers => {
    const map = {};
    answers.forEach(line => {
        line.split('').forEach(c => {
            if (c in map) {
                map[c]++;
            } else {
                map[c] = 1;
            }
        });
    });
    const result = new Set();
    for (let [key, value] of Object.entries(map)) {
        if (value == answers.length) {
            result.add(key);
        }
    }
    return result;
};
