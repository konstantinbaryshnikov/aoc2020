import {readFile} from '../common/input.js';

export const parseInput = () => readFile('./input', s => s.split(/\r?\n/).filter(s => s.length), /\r?\n\r?\n/);
