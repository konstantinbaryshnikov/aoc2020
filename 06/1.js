import {parseInput} from './input.js';
import {parseGroup} from './group.js';

const count = parseInput().reduce((a, c) => a + parseGroup(c).size, 0);
console.log(count);
