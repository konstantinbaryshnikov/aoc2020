import {parseInput} from './input.js';
import {parseGroup2} from './group.js';

const count = parseInput().reduce((a, c) => a + parseGroup2(c).size, 0);
console.log(count);
