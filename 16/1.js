import {readInput} from './input.js';
const d = readInput();
let invalid = 0;
const test = v => {
    let isValid = 0;
    for (let rr of d.ranges) {
        for (let r of rr) {
            let [a,b]=r;
            if (v >= a && v <= b) {
                isValid = 1;
            }
        }
    }
    if (!isValid) {
        invalid += v;
    }
};
d.nearby.forEach(line => line.forEach(v => test(v)))
console.log(invalid);
