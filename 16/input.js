import {readFile} from '../common/input.js';
export const readInput = () => {
    const lines = readFile('./input', s => s.trim());
    const r = {
        ranges: [],
        my: null,
        nearby: [],
        key: [],
    };
    let mode = 0;
    for (let line of lines) {
        switch (line) {
            case 'your ticket:':
                mode = 1;
                break;
            case 'nearby tickets:':
                mode = 2;
                break;
            case '':
                break;
            default:
                switch (mode) {
                    case 0:
                        const m = line.match(/: (\d+)-(\d+) or (\d+)-(\d+)/);
                        if (line.match(/departure/)) {
                            r.key.push(r.ranges.length);
                        }
                        r.ranges.push([
                            [m[1]|0, m[2]|0],
                            [m[3]|0, m[4]|0],
                        ]);
                        break;
                    case 1:
                        r.my = line.split(',').map(v => v|0);
                        break;
                    case 2:
                        r.nearby.push(line.split(',').map(v => v|0));
                        break;
                }
        }
    }
    return r
};
