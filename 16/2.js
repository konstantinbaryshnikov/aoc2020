import {readInput} from './input.js';
const d = readInput();
const test = v => {
    let isValid = 0;
    for (let rr of d.ranges) {
        for (let r of rr) {
            let [a,b]=r;
            if (v >= a && v <= b) {
                return true;
            }
        }
    }
    return false;
};
const valid = d.nearby.filter(line => line.every(v => test(v)))
const map = [];
for (let i = 0; i < d.ranges.length; i++) {
    let rr = d.ranges[i];
    for (let j = 0; j < d.ranges.length; j++) {
        let isValid = valid.every(line => rr.some(r => line[j] >= r[0] && line[j] <= r[1]));
        if (isValid) {
            if (!map[i]) map[i] = [];
            map[i].push(j);
        }
    }
}
let done;
do {
    done = true;
    for (let i = 0; i < map.length; i++) {
        if (map[i].length == 1) {
            let v = map[i][0];
            for (let j = 0; j < map.length; j++) {
                if (j != i) {
                    let idx = map[j].indexOf(v);
                    if (idx >= 0) map[j].splice(idx, 1);
                }
            }
        } else {
            done = false;
        }
    }
} while (!done);
let s = 1;
for (let i of d.key) {
    s *= d.my[map[i][0]];
}
console.log(s);
