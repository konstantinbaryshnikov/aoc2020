import {load} from './input.js';

const food = load();

const allergens = {};
const count = {};

for (let f of food) {
    const [is, as] = f;
    const ingredients = new Set(is);
    for (let i of ingredients) {
        count[i] = (count[i] || 0) + 1;
    }
    for (let a of as) {
        if (a in allergens) {
            allergens[a] = new Set(is.filter(i => allergens[a].has(i)));
        } else {
            allergens[a] = new Set(is);
        }
    }
}

const unsafe = new Set(Object.values(allergens).flatMap(c => [...c]));
const safe = Object.keys(count).filter(i => !unsafe.has(i));

// part 1 solution
console.log(safe.reduce((c, k) => c + count[k], 0));

const kv = Object.entries(allergens);
kv.sort((a, b) => a[1].size - b[1].size);
const used = new Set();
for (let i = 0; i < kv.length; i++) {
    const ing = [...kv[i][1]].filter(v => !used.has(v))[0];
    used.add(ing);
    kv[i][1] = ing;
}
kv.sort((a, b) => a[0].localeCompare(b[0]));

// part 2 solution
console.log(kv.map(v => v[1]).join(','));
