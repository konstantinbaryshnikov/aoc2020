import {readFile} from '../common/input.js';

export const load = () => readFile('./input', s => {
    const m = s.match(/^(.*) \(contains (.*)\)$/);
    const ing = m[1].split(' ');
    const all = m[2].split(', ');
    return [ing, all];
});
