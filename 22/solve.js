import {load} from './input.js';

const score = deck => deck.reduce((c, v, i) => c + (deck.length - i) * v, 0);

const play = decks => {
    let loser = -1;
    do {
        const cards = [0, 1].map(i => decks[i].shift());
        const rw = cards[0] > cards[1] ? 0 : 1;
        (rw ? cards.reverse() : cards).forEach(v => decks[rw].push(v));
        loser = decks.findIndex(deck => deck.length === 0);
    } while (loser < 0);
    const winner = +!loser;
    return decks[winner];
};

const play2 = decks => {
    let loser = -1;
    const hashes = new Set();
    do {
        const hash = JSON.stringify(decks);
        if (hashes.has(hash)) {
            loser = 1;
            break;
        }
        hashes.add(hash);
        const cards = [0, 1].map(i => decks[i].shift());
        let rw;
        if ([0, 1].every(i => decks[i].length >= cards[i])) {
            rw = play2(decks.map((deck, i) => deck.slice(0, cards[i])))[0];
        } else {
            rw = cards[0] > cards[1] ? 0 : 1;
        }
        (rw ? cards.reverse() : cards).forEach(v => decks[rw].push(v));
        loser = decks.findIndex(deck => deck.length === 0);
    } while (loser < 0);
    const winner = +!loser;
    return [winner, decks[winner]];
};

// Part 1
console.log(score(play(load())));

// Part 2
console.log(score(play2(load())[1]));
