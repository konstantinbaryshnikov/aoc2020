import {readFile} from '../common/input.js';

export const load = () => {
    const decks = [[], []];
    let idx = -1;
    for (let line of readFile('./input', s => s.trim())) {
        if (line.match(/Player/)) {
            idx++;
        } else {
            decks[idx].push(line | 0);
        }
    }
    return decks;
};
