import {readFile} from '../common/input.js';
import {parse, test} from './policy.js';

export const readInput = () => readFile('./input', s => {
    const [policySpec, password] = s.split(/:\s*/);
    return {
        policy: parse(policySpec),
        password,
    };
});
