import {test2} from './policy.js';
import {readInput} from './input.js';

const input = readInput();
let validCount = 0;
let invalidCount = 0;
for (let line of input) {
    const valid = test2(line.policy, line.password);
    if (valid) {
        validCount++;
    } else {
        invalidCount++;
    }
}

console.log(validCount, invalidCount);
