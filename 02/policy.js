export const parse = policySpec => {
    const m = policySpec.trim().match(/^([0-9]+)-([0-9]+)\s([a-z])$/);
    if (!m) {
        throw new Error("Invalid policySpec: " + policySpec);
    }
    return {
        min: +m[1],
        max: +m[2],
        chr: m[3],
    };
};

export const test = (policy, password) => {
    let m = 0;
    for (let i = 0; i < password.length; i++) {
        if (password.charAt(i) == policy.chr) {
            m++;
        }
        if (m > policy.max) {
            return false;
        }
    }
    return m >= policy.min;
}

export const test2 = (policy, password) => {
    return !!(
        (password.charAt(policy.min - 1) == policy.chr)
        ^
        (password.charAt(policy.max - 1) == policy.chr)
    );
}
