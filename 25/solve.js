const rem = 20201227;

const nextTransform = (subj, value) => (value * subj) % rem;

const transform = (subj, loopSize, value = 1) => {
    while (loopSize-- > 0) {
        value = nextTransform(subj, value);
    }
    return value;
};

const findLoopSize = (subj, expectedResult) => {
    let loopsize = 1;
    let value = 1;
    while (true) {
        value = nextTransform(subj, value);
        if (value === expectedResult) {
            return loopsize;
        }
        loopsize++;
    };
    return loopsize;
};

const initialSubject = 7;

const cardPubkey = 2959251;
const doorPubkey = 4542595;

const cardLoopSize = findLoopSize(initialSubject, cardPubkey);
const doorLoopSize = findLoopSize(initialSubject, doorPubkey);

const cardEncKey = transform(doorPubkey, cardLoopSize);
const doorEncKey = transform(cardPubkey, doorLoopSize);

console.assert(cardEncKey === doorEncKey, 'encryption keys must be equal');
console.log(cardEncKey, doorEncKey);
