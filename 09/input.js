import {readFile} from '../common/input.js';
export const readInput = () => readFile('./input', v => v | 0);
