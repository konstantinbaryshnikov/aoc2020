import {readInput} from './input.js';
import {findPair} from './util.js';

const preamble = 25;
const input = readInput();
for (let i = preamble; i < input.length; i++) {
    if (!findPair(input.slice(i - preamble, i), input[i])) {
        console.log(input[i]);
        break;
    }
}
