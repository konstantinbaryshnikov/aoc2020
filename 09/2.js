import {readInput} from './input.js';
import {findPair, findSubarray} from './util.js';

const preamble = 25;
const input = readInput();
for (let i = preamble; i < input.length; i++) {
    if (!findPair(input.slice(i - preamble, i), input[i])) {
        const s = findSubarray(input, input[i]);
        s.sort((a, b) => a - b);
        console.log(s[0] + s[s.length - 1]);
        break;
    }
}
