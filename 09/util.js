export const findPair = (a, s) => {
    a.sort((a, b) => a - b);
    let i = 0;
    let j = a.length;

    while (i < j) {
        const sum = a[i] + a[j];
        if (sum == s) {
            return [i, j];
        }
        if (sum < s) {
            i++;
        } else {
            j--;
        }
    }
};

export const findSubarray = (a, s) => {
    for (let i = 0; i < a.length; i++) {
        let sum = a[i];
        for (let j = i + 1; j < a.length; j++) {
            sum += a[j];
            if (sum == s) {
                return a.slice(i, j);
            }
            if (sum > s) {
                break;
            }
        }
    }
};
