const s = '7,12,1,0,16,2';
const lim = 30000000 // 2020;
const init = s.split(',').map(v=>+v);
const tracker = new Map();
let i = 1;
while (i <= init.length - 1) {
    tracker.set(init[i-1], i);
    i++;
}
let last = init[init.length - 1];
while (i < lim) {
//    console.log(i, last, tracker);
    let next;
    const t = tracker.get(last);
    if (t != null) {
        next = i - t;
    } else {
        next = 0;
    }
    tracker.set(last, i);
    i++;
    last = next;
}
console.log(last);
