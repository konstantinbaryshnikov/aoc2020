import {readFile} from '../common/input.js';

// http://devmag.org.za/2013/08/31/geometry-with-hex-coordinates/
const dir = {
    e:  [1,0,-1],
    ne: [0,1,-1],
    nw: [-1,1,0],
    w:  [-1,0,1],
    sw: [0,-1,1],
    se: [1,-1,0],
};

const load = () => {
    const re = new RegExp('(' + Object.keys(dir).join('|') + ')', 'g');
    return readFile('./input', s => s.match(re));
};

const move = (pos, dc) => {
    dc.forEach((v, i) => pos[i] += v);
    return pos;
}

const walk = (directions, pos) => {
    for (let d of directions) {
        move(pos, dir[d]);
    }
    return pos;
}

const key = pos => pos.join(':');
const keyToPos = key => key.split(':').map(v => v | 0);

const part1 = input => {
    const map = new Map();
    for (let directions of input) {
        const k = key(walk(directions, [0, 0, 0]));
        if (map.has(k)) {
            map.delete(k);
        } else {
            map.set(k, 1);
        }
    }
    console.log(map.size);
};

const part2 = (input, days) => {
    const map = new Map();
    const adjacentBlackCount = pos => Object.values(dir).reduce((c, dc) => c + (map.get(key(move([...pos], dc))) || 0), 0);
    const countBlack = () => Array.from(map).reduce((c, [, v]) => c + v, 0);
    const mapSet = (pos, black) => {
        const k = key(pos);
        map.set(k, black === undefined ? +!map.get(k) : black);
        Object.values(dir).forEach(dc => {
            const k = key(move([...pos], dc));
            if (!map.has(k)) {
                map.set(k, 0);
            }
        });
    };
    for (let directions of input) {
        mapSet(walk(directions, [0, 0, 0]));
    }
    for (let day = 0; day < days; day++) {
        const flips = [];
        for (let [k, black] of map) {
            const pos = keyToPos(k);
            const adjBlack = adjacentBlackCount(pos);
            if ((black && (adjBlack == 0 || adjBlack > 2)) || (!black && adjBlack == 2)) {
                flips.push(pos);
            }
        }
        flips.forEach(pos => mapSet(pos));
    }
    console.log(countBlack());
};

part1(load());
part2(load(), 100);
