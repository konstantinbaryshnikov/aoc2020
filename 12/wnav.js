export class WaypointNav {

    constructor() {
        this.x = 0;
        this.y = 0;
        this.wx = 10;
        this.wy = 1;
        this.direction = 'E';
    }

    navigate(path) {
        for (let step of path) {
            this.step(...step);
        }
    }

    step(direction, distance) {
        switch (direction) {
            case 'F':
                this.moveShip(distance);
                return;
            case 'L':
                this.rotate(-distance);
                return;
            case 'R':
                this.rotate(distance);
                return;
        }
        distance && this['step' + direction](distance);
    }

    rotate(d) {
        let dir = ((d / 90) + 4) % 4;
        while (dir-- > 0) {
            let wx = this.wx;
            let wy = this.wy;
            this.wx = wy;
            this.wy = -wx;
        }
    }

    moveShip(times) {
        this.x += (this.wx * times);
        this.y += (this.wy * times);
    }

    stepN(distance) {
        this.wy += distance;
    }

    stepS(distance) {
        this.wy -= distance;
    }

    stepW(distance) {
        this.wx -= distance;
    }

    stepE(distance) {
        this.wx += distance;
    }

    md() {
        return Math.abs(this.x) + Math.abs(this.y);
    }

}
