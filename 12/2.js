import {loadPath} from './input.js';
import {WaypointNav} from './wnav.js';

const path = loadPath();
const nav = new WaypointNav();
nav.navigate(path);
console.log(nav.md());
