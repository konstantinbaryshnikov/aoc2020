import {loadPath} from './input.js';
import {Nav} from './nav.js';

const path = loadPath();
const nav = new Nav();
nav.navigate(path);
console.log(nav.md());
