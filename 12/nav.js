export class Nav {

    constructor() {
        this.x = 0;
        this.y = 0;
        this.direction = 'E';
    }

    navigate(path) {
        for (let step of path) {
            this.step(...step);
        }
    }

    step(direction, distance) {
        //console.log(direction, distance, this.direction);
        switch (direction) {
            case 'F':
                direction = this.direction;
                break;
            case 'L':
                this.direction = this.rotate(-distance / 90);
                distance = 0;
                break;
            case 'R':
                this.direction = this.rotate(distance / 90);
                distance = 0;
                break;
        }
        distance && this['step' + direction](distance);
        //console.log('->', this.direction, this.x, this.y);
    }

    rotate(d) {
        //console.log('% rotate', d);
        return Nav.directions[(Nav.directions.indexOf(this.direction) + Nav.directions.length + d) % Nav.directions.length];
    }

    stepN(distance) {
        this.y += distance;
    }

    stepS(distance) {
        this.y -= distance;
    }

    stepW(distance) {
        this.x -= distance;
    }

    stepE(distance) {
        this.x += distance;
    }

    md() {
        return Math.abs(this.x) + Math.abs(this.y);
    }

}

Nav.directions = ['N', 'E', 'S', 'W'];
