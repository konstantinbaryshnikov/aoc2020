import {readFile} from '../common/input.js';

export const loadPath = () => readFile('./input', s => [s.charAt(0), s.substr(1) | 0]);
