import {Registry} from './bag.js';

const lines = `light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`.split("\n");

const reg = new Registry;
for (let line of lines) {
    reg.addBySpec(line);
}

const entrypoint = reg.get('shiny', 'gold');
console.log(Array.from(entrypoint.getContainers()).map(c => c.id()));
console.log(Array.from(entrypoint.getContainments()).map(c => c.count + ' ' + c.bag.id()));
