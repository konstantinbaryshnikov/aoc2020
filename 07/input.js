import {Registry} from './bag.js';
import {readFile} from '../common/input.js';

export const loadBags = () => readFile('./input', s => s.trim()).reduce((acc, line) => acc.addBySpec(line), new Registry);

