const bagId = (type, color) => type + '_' + color;

export class Registry {

    constructor() {
        this.items = {};
    }

    get(type, color) {
        const id = bagId(type, color);
        if (!(id in this.items)) {
            this.items[id] = new Bag(type, color);
        }
        return this.items[id];
    }

    addBySpec(spec) {
        const m = spec.match(/(\w+) (\w+) bags? contains? (.*)./);
        const type = m[1];
        const color = m[2];
        const bag = this.get(type, color);
        if (m[3] != 'no other bags') {
            const containments = m[3].split(', ');
            for (let c of containments) {
                const m = c.match(/(\d+) (\w+) (\w+) bags?/);
                const count = m[1] | 0;
                const type = m[2];
                const color = m[3];
                const subBag = this.get(type, color);
                bag.addContainment(new Containment(count, subBag));
            }
        }
        return this;
    }

}

export class Bag {

    constructor(type, color) {
        this.type = type;
        this.color = color;
        this.containments = {};
        this.containers = {};
    }

    addContainment(containment) {
        this.containments[containment.bag.id()] = containment;
        containment.bag.containers[this.id()] = this;
    }

    getContainments(mult) {
        mult = mult || 1;
        let containments = Object.values(this.containments);
        for (let c of Object.values(this.containments)) {
            containments = containments.concat(c.bag.getContainments(c.count));
        }
        return containments.map(c => new Containment(c.count * mult, c.bag));
    }

    getContainers() {
        const containers = new Set();
        for (let container of Object.values(this.containers)) {
            containers.add(container);
            container.getContainers().forEach(c => containers.add(c));
        }
        return containers;
    }

    id() {
        return bagId(this.type, this.color);
    }

}

export class Containment {

    constructor(count, bag) {
        this.count = count;
        this.bag = bag;
    }

}
