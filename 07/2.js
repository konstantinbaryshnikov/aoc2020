import {Registry} from './bag.js';
import {loadBags} from './input.js';

const reg = loadBags();
const entrypoint = reg.get('shiny', 'gold');
console.log(entrypoint.getContainments().reduce((a, c) => a + c.count, 0));
