import {Registry} from './bag.js';
import {loadBags} from './input.js';

const reg = loadBags();
const entrypoint = reg.get('shiny', 'gold');
console.log(entrypoint.getContainers().size);

