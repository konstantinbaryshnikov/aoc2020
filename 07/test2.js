import {Registry} from './bag.js';

const lines = `shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.`.split("\n");

const reg = new Registry;
for (let line of lines) {
    reg.addBySpec(line);
}

const entrypoint = reg.get('shiny', 'gold');
console.log(entrypoint.getContainments().reduce((a, c) => a + c.count, 0));
