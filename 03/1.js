import {loadMap} from './input.js';
import {Map, Slope} from './map.js';

const map = loadMap();
const slope = new Slope(3, 1);

const acc = {
    [Map.TREE]: 0,
    [Map.OPEN]: 0,
};

while (map.walk(slope)) {
    acc[map.curr()]++;
}

console.log(acc);
