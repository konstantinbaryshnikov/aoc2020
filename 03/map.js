export class Map {

    constructor(map) {
        this.map = map;
        this.width = map[0].length;
        this.height = map.length;
        this.reset();
    }

    reset() {
        this.x = 0;
        this.y = 0;
    }

    walk(slope) {
        this.x += slope.x;
        this.y += slope.y;
        return this.y < this.height;
    }

    curr() {
        return this.at(this.x, this.y);
    }

    at(x, y) {
        x = x % this.width;
        return this.map[y][x];
    }

}

export class Slope {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

}

Map.TREE = '#';
Map.OPEN = '.';
