import {loadMap} from './input.js';
import {Map, Slope} from './map.js';

const map = loadMap();
const slopes = [
    new Slope(1, 1),
    new Slope(3, 1),
    new Slope(5, 1),
    new Slope(7, 1),
    new Slope(1, 2),
];

const newAcc = () => ({
    [Map.TREE]: 0,
    [Map.OPEN]: 0,
});

const res = [];

for (const slope of slopes) {
    const acc = newAcc();
    map.reset();
    while (map.walk(slope)) {
        acc[map.curr()]++;
    }
    res.push(acc);
}

//console.log(res);

const m = res.reduce((c, a) => c * a[Map.TREE], 1);
console.log(m);
