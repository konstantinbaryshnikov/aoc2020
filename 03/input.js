import {readFile} from '../common/input.js';
import {Map} from './map.js';

export const loadMap = () => new Map(readFile('./input', s => s.trim()));
