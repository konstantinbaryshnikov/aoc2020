import {readProgram} from './input.js';
import {MemMaskVm} from './memmask.js';

const program = readProgram();
const vm = new MemMaskVm();
vm.run(program);
console.log(vm.sum());
