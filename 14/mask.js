export class MaskVm {

    constructor() {
        this.memory = [];
        this.mask = [];
    }

    run(program) {
        for (let line of program) {
            let m;
            const [op, val] = line.split(/ *= */);
            if (op == 'mask') {
                this.setMask(val);
                continue;
            }
            if (m = op.match(/^mem\[([0-9]+)\]$/)) {
                let addr = m[1];
                this.setMem(+addr, +val);
                continue;
            }
            throw new Error("Invalid line: " + line);
        }
    }

    setMem(addr, val) {
        const mval = this.applyMask(val);
        this.memory[addr] = mval;
    }

    applyMask(val) {
        const s = val.toString(2).split('').reverse();
        while (s.length < 36) s.push('0');
        for (let [op, idx] of this.mask) {
            s[idx] = op;
        }
        const r = parseInt(s.reverse().join(''), 2);
        return r;
    }

    sum() {
        return this.memory.reduce((c, v) => c + (v || 0), 0);
    }

    setMask(maskStr) {
        this.mask = maskStr.split('').reverse().reduce((c, v, i) => {
            switch (v) {
                case '1':
                case '0':
                    c.push([v, i]);
            }
            return c;
        }, []);
    }

}
