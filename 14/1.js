import {readProgram} from './input.js';
import {MaskVm} from './mask.js';

const program = readProgram();
const vm = new MaskVm();
vm.run(program);
console.log(vm.sum());
