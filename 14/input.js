import {readFile} from '../common/input.js';

export const readProgram = () => readFile('./input', s => s.trim());
