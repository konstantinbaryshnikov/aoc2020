export class MemMaskVm {

    constructor() {
        this.memory = {};
        this.mask = [];
    }

    run(program) {
        for (let line of program) {
            let m;
            const [op, val] = line.split(/ *= */);
            if (op == 'mask') {
                this.setMask(val);
                continue;
            }
            if (m = op.match(/^mem\[([0-9]+)\]$/)) {
                let addr = m[1];
                this.setMem(+addr, +val);
                continue;
            }
            throw new Error("Invalid line: " + line);
        }
    }

    setMem(addr, val) {
        const s = addr.toString(2).split('').reverse();
        while (s.length < 36) {
            s.push('0');
        }
        for (let i of this.mask.ones) {
            s[i] = '1';
        }
        const queue = [s];
        const inverted = (as, i) => {
            const r = Array.from(as);
            r[i] = r[i] == '0' ? '1' : '0';
            return r;
        };
        for (let i of this.mask.floats) {
            let l = queue.length;
            for (let j = 0; j < l; j++) {
                queue.push(inverted(queue[j], i));
            }
        }
        for (let i of queue) {
            const a = parseInt(i.reverse().join(''), 2);
            this.memory[a] = val;
        }
    }

    sum() {
        let s = 0;
        for (let i of Object.values(this.memory)) {
            s += i;
        }
        return s;
    }

    setMask(maskStr) {
        this.mask = maskStr.split('').reverse().reduce((c, v, i) => {
            switch (v) {
                case '1':
                    c.ones.push(i);
                    break;
                case 'X':
                    c.floats.push(i);
                    break;
            }
            return c;
        }, {ones: [], floats: []});
    }

}
